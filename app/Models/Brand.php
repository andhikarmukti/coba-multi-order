<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function category()
    {
        return $this->hasMany(Category::class);
    }

    public function variant()
    {
        return $this->hasManyThrough(Variant::class, Category::class);
    }
}
