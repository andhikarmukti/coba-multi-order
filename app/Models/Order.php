<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function staff()
    {
        $this->hasMany(Staff::class);
    }

    public function courier()
    {
        $this->belongsTo(Courier::class, 'courier_id');
    }
}
