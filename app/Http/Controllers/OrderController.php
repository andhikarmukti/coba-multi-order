<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Courier;
use App\Models\Product;
use App\Models\Staff;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $this->authorize('admin'); // GATE untuk hanya admin yang boleh akses

        $items     = Order::all()->unique('order_number');
        $products  = Product::all();
        $staffs    = Staff::all();
        $product   = new Product;
        $staff     = new Staff;
        $title     = 'Penjualan';
        $couriers  = Courier::all();

        if ($request->order_number) {
            $order_number = Order::where('order_number', $request->order_number)->pluck('order_number');
            $orders = Order::join('products', 'orders.product_id', '=', 'products.id')
                ->where('orders.order_number', $request->order_number)
                ->get();
            $person_responsible = Staff::find($orders->first()->person_responsible);
            $quality_control = Staff::find($orders->first()->quality_control);
            $packing = Staff::find($orders->first()->packing);
            $product = Product::find($orders->first()->product_id);

            return response()->json([
                'orders' => $orders,
                'person_responsible' => $person_responsible,
                'quality_control' => $quality_control,
                'packing' => $packing,
                'product' => $product
            ]);
        }

        return view('order.create', compact(
            'items',
            'products',
            'staffs',
            'product',
            'staff',
            'couriers',
            'title'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'recipient' => 'required',
            'person_responsible' => 'required',
            'order_number' => 'required',
            'receipt_number' => 'required',
            'quality_control' => 'required',
            'packing' => 'required',
            'status' => 'required',
            'product_id_0' => 'required',
            'quantity_0' => 'required',
            'courier_id' => 'required'
        ]);

        $length = (count($request->all()) - 8);
        // dd($request->all());
        // dd($length);
        for ($i = 0; $i < $length; $i++) {
            if (request('product_id_' . $i)) {
                Order::create([
                    'recipient' => $request->recipient,
                    'person_responsible' => $request->person_responsible,
                    'order_number' => $request->order_number,
                    'receipt_number' => $request->receipt_number,
                    'quality_control' => $request->quality_control,
                    'packing' => $request->packing,
                    'status' => $request->status,
                    'courier_id' => $request->courier_id,
                    'product_id' => request('product_id_' . $i),
                    'quantity' => request('quantity_' . $i),
                    'product_price' => Product::where('id', request('product_id_' . $i))->first()->product_price,
                ]);
            }
        }
        return back()->with('store', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $request->validate([
            'photo_proof' => 'required'
        ]);

        Order::where('order_number', $request->order_number)->update([
            'photo_proof' => $request->photo_proof,
            'status' => 'selesai'
        ]);

        return back()->with('update_photo_proof', 'Berhasil update bukti foto');
    }

    public function jqueryGetIdOrder(Request $request)
    {
        $data_order = Order::where('order_number', $request->order_number)->first();
        return response()->json($data_order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
    public function tes()
    {
    }
}
