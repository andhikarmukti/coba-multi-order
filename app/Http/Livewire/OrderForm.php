<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\Staff;
use App\Models\Product;
use Livewire\Component;
use Illuminate\Http\Request;

class OrderForm extends Component
{
    public $items;
    public $products;
    public $staffs;
    public $product;
    public $staff;

    public $length;
    public $person_responsible;
    public $recipient;
    public $order_number;
    public $receipt_number;
    public $quality_control;
    public $packing;
    public $status;

    public function mount()
    {
        $this->items = Order::where('status', 'packing')->get()->unique('order_number');
        $this->products = Product::all();
        $this->staffs = Staff::all();
        $this->product = new Product;
        $this->staff = new Staff;

        $this->items = Order::where('status', 'packing')->get()->unique('order_number');
        // $this->length = (count($request->all()) - 8);
    }

    public function render()
    {
        return view('livewire.order-form',[
            'items' => $this->items,
            'products' => $this->products,
            'staffs' => $this->staffs,
            'product' => $this->product,
            'staff' => $this->staff
        ]);
    }

    public function storeOrder()
    {
        dd($this->length);
        for ($i = 0; $i < $this->length; $i++) {
            if (request('product_id_' . $i)) {
                Order::create([
                    'recipient' => $this->recipient,
                    'person_responsible' => $this->person_responsible,
                    'order_number' => $this->order_number,
                    'receipt_number' => $this->receipt_number,
                    'quality_control' => $this->quality_control,
                    'packing' => $this->packing,
                    'status' => $this->status,
                    'product_id' => request('product_id_' . $i),
                    'quantity' => request('quantity_' . $i),
                    'product_price' => Product::where('id', request('product_id_' . $i))->first()->product_price,
                ]);
            }
        }
        return back()->with('store', 'Berhasil menambah data');
    }
}
