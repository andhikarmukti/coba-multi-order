<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use App\Models\Category;
use Livewire\Component;

class CategoryCreateForm extends Component
{
    public $brands, $brand_id, $category_name;

    public function mount()
    {
        $this->brands = Brand::all();
    }

    public function render()
    {
        return view('livewire.category-create-form');
    }

    protected $rules = [
        'category_name' => 'required',
        'brand_id' => 'required',
    ];

    public function store()
    {
        $this->validate();

        Category::create([
            'category_name' => $this->category_name,
            'brand_id' => $this->brand_id,
        ]);

        $this->emit('stored_category');

        session()->flash('store', 'Berhasil menambahkan data');
    }
}
