<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use App\Models\Product;
use Livewire\Component;

class BrandCreateTable extends Component
{
    public $brand_id;
    public $brand_name;
    public $selected;
    public $action;
    public $editMode = false;

    protected $listeners = [
        'brandStored',
        'updatedBrand'
    ];

    public function updatedBrand()
    {
        session()->flash('update', 'Berhasil update data brand');
    }

    public function mount()
    {
    }

    public function render()
    {
        return view('livewire.brand-create-table', [
            'brands' => Brand::where('enable', 1)->get(),
            'brand_name' => $this->brand_name,
            'brand_id' =>  $this->selected
        ]);
    }

    public function brandStored()
    {
        session()->flash('store', 'Berhasil menambahkan data');
        // jika ingin diberikan sesuatu bisa diisi, jika tidak yasudah. Karena file ini sudah terpanggil menggunakan listeners
    }

    public function delete()
    {
        $check = Product::where('brand_id', $this->selected)->count(); // pengecekan apakah brand yang akan dihapus ada di tabel product
        if($check > 0){
            Brand::find($this->selected)->update([
                'enable' => 0
            ]);
            return session()->flash('delete', 'Berhasil menghapus data');
        }

        Brand::find($this->selected)->delete();

        // $this->dispatchBrowserEvent('closeModal');

        session()->flash('delete', 'Berhasil menghapus data');
    }

    public function selectItem($selected, $action)
    {
        $this->selected = $selected;
        $this->action = $action;

        $this->emit('brandId', $this->selected);

        if($action == 'delete'){
            $this->dispatchBrowserEvent('openModalDelete');
        }else{
            $this->brand_name = Brand::find($this->selected)->brand_name;
            $this->emit('brandName', $this->brand_name);
            $this->dispatchBrowserEvent('openModalEdit');
        }

    }

}
