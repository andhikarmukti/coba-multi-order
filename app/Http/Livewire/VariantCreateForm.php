<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Variant;
use Livewire\Component;

class VariantCreateForm extends Component
{
    public $variant_name, $categories, $category_id = 'Open this select menu';

    public function mount()
    {
        $this->categories = Category::all();
    }

    public function render()
    {
        return view('livewire.variant-create-form');
    }

    public function store()
    {
        Variant::create([
            'category_id' => $this->category_id,
            'variant_name' => $this->variant_name
        ]);

        session()->flash('store', 'Berhasil menambahkan data');
        $this->emit('stored_variant');
    }
}
