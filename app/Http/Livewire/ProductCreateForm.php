<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Variant;
use Livewire\Component;
use App\Models\Category;
use Illuminate\Http\Request;

class ProductCreateForm extends Component
{
    public $brandInput;
    public $categoryInput;

    public $brands;
    public $categories;
    public $variants;

    public $categoryForm;
    public $variantForm;

    public $product_name;
    public $brandId;
    public $categoryId;
    public $variant_id;
    public $capital_price;
    public $product_price;

    public function mount()
    {
        $this->brands = Brand::all();

        $this->categories = collect();
        $this->variants = collect();

        $this->categoryForm = null;
        $this->variantForm = null;
    }

    public function render()
    {
        return view('livewire.product-create-form');
    }

    public function updatedbrandId()
    {
        $this->categories = Category::where('brand_id', $this->brandId)->get();
        $this->categoryForm = 1;
    }

    public function updatedcategoryId()
    {
        $this->variants = Variant::where('category_id', $this->categoryId)->get();
        $this->variantForm = 1;
    }

    protected $rules = [
        'product_name' => 'required',
        'brandId' => 'required',
        'categoryId' => 'required',
        'variant_id' => 'required',
        'capital_price' => 'required',
        'product_price' => 'required'
    ];

    public function storeProduct()
    {
        $this->validate();

        $product = Product::create([
            'product_name' => $this->product_name,
            'brand_id' => $this->brandId,
            'category_id' => $this->categoryId,
            'variant_id' => $this->variant_id,
            'capital_price' => str_replace('.', '', $this->capital_price),
            'product_price' => str_replace('.', '', $this->product_price)
        ]);

        $this->resetInput();

        $this->emit('productStored', $product);

        // session()->flash('store', 'Berhasil menambahkan data');
    }

    private function resetInput()
    {
        $this->product_name = null;
        $this->brandId = null;
        $this->categoryForm = null;
        $this->variantForm = null;
        $this->capital_price = null;
        $this->product_price = null;
    }
}
