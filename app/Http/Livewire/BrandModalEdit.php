<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use Livewire\Component;

class BrandModalEdit extends Component
{
    public $brandId;
    public $brand_name;

    protected $listeners = [
        'brandId',
        'brandName'
    ];

    public function brandId($brandId)
    {
        $this->brandId = $brandId;
    }

    public function brandName($brandName)
    {
        $this->brand_name = $brandName;
    }

    public function render()
    {
        return view('livewire.brand-modal-edit');
    }

    public function update()
    {
        Brand::find($this->brandId)->update([
            'brand_name' => $this->brand_name
        ]);

        $this->emit('updatedBrand');
    }
}
