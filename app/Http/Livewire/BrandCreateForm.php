<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use Livewire\Component;

class BrandCreateForm extends Component
{
    public $brand_name;
    public $editMode = false;
    public $brand_id;

    protected $listeners = [
        'editMode'
    ];

    public function editMode($editMode)
    {
        $this->editMode = $editMode;
    }

    public function render()
    {
        return view('livewire.brand-create-form');
    }

    protected $rules = [
        'brand_name' => 'required'
    ];

    public function storeBrand()
    {
        $this->validate();

        Brand::create([
            'brand_name' => $this->brand_name
        ]);

        $this->resetInput();

        $this->emit('brandStored');

        session()->flash('store', 'Berhasil manambahkan brand');
    }

    public function resetInput()
    {
        $this->brand_name = null;
    }
}
