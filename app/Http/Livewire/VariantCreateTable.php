<?php

namespace App\Http\Livewire;

use App\Models\Variant;
use Livewire\Component;

class VariantCreateTable extends Component
{
    protected $listeners = [
        'stored_variant' => 'render'
    ];

    public function mount()
    {
    }

    public function render()
    {
        return view('livewire.variant-create-table', [
            'variants' => Variant::join('categories', 'variants.category_id', '=', 'categories.id')
                ->select('categories.*', 'variants.*')
            // ->where('variants.enable', 1)->get()
        ]);
    }
}
