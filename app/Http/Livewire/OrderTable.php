<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Livewire\Component;

class OrderTable extends Component
{
    public $items;

    public function mount()
    {
        $this->items = Order::where('status', 'packing')->get()->unique('order_number');
    }

    public function render()
    {
        return view('livewire.order-table',['items' => $this->items]);
    }



    public function resetInput()
    {

    }
}
