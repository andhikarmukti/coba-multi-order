<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Variant;
use Livewire\Component;
use App\Models\Category;

class ProductCreateTable extends Component
{
    // public $products;
    // public $brands;

    // public $brand;
    // public $category;
    // public $variant;

    public $search;

    public function mount()
    {
        // $this->products   = Product::all();
        // $this->brands     = Brand::all();

        // $this->brand    = new Brand;
        // $this->category = new Category;
        // $this->variant  = new Variant;

        $this->search = null;
    }

    protected $listeners = [
        'productStored'
    ];

    public function render()
    {
        return view('livewire.product-create-table', [
            'products' => Product::all(),
            'brands' => Brand::all(),
            'brand' => new Brand,
            'category' => new Category,
            'variant' => new Variant
        ]);
        // langsung taro variablenya di render supaya bisa langsung jalan ketika submit data baru [sebab kalau ditaro di mount, funsi emet hanya menjalankan rendernya saja tanpa melalui mount. sehingga tidak terjadi perubahan secara realtime ketika submit product baru]
    }

    public function updatedSearch()
    {
        $this->products = Product::where('product_name', 'like', '%' . $this->search . '%')->get();
    }

    public function productStored($product)
    {
        // dd($product);
        // session()->flash('store', 'Berhasil menambahkan data');
    }
}
