<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;

class CategoryCreateTable extends Component
{
    public $category_id;

    protected $listeners = [
        'stored_category' => 'render',
        'category_edited'
    ];

    public function category_edited()
    {
        session()->flash('category_edited', 'Berhasil merubah data');
    }

    public function render()
    {
        return view('livewire.category-create-table',[
            'categories' => Category::join('brands', 'categories.brand_id', '=', 'brands.id')->select('brands.*', 'categories.*')->get()
        ]);
    }

    public function modal($category_id, $brand_id, $action)
    {
        if($action == 'edit'){
            $this->dispatchBrowserEvent('openModalEdit');
            $this->emit('category_id_edit', $category_id, $brand_id);
        }else{
            $this->category_id = $category_id;
            $this->dispatchBrowserEvent('openModalDelete');
        }
    }

    public function delete()
    {
        Category::find($this->category_id)->delete();
        session()->flash('deleted_item', 'Data berhasil dihapus');
    }
}
