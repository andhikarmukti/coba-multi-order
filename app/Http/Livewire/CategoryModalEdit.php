<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use Livewire\Component;
use App\Models\Category;

class CategoryModalEdit extends Component
{
    public $category_name, $category_id_edit, $brand_id, $brand_id_edit, $brand_name;

    protected $listeners = [
        'category_id_edit'
    ];

    public function mount()
    {
        // $this->brand_id = 10;
    }

    public function category_id_edit($category_id, $brand_id_edit)
    {
        $this->category_id_edit = $category_id;
        $this->category_name = Category::find($this->category_id_edit)->category_name;
        $this->brand_id = Brand::find($brand_id_edit)->id; // livewire akan melakukan select sendiri di dropdown, tinggal sesuaikan nilai wire:modelnya (brand_id) yang akan berubah jika klik tombol edit
    }

    public function render()
    {
        return view('livewire.category-modal-edit', [
            'brands' => Brand::all()
        ]);
    }

    public function update()
    {
        Category::find($this->category_id_edit)->update([
            'category_name' => $this->category_name,
            'brand_id' => $this->brand_id
        ]);

        $this->dispatchBrowserEvent('closeModalEdit');
        $this->emit('category_edited');
    }
}
