@extends('layout.main')

@section('section')
<div class="container">
    <div class="row">
        <div class="col-4 mx-auto mt-5">
            <div class="card p-4 shadow">
                <livewire:brand-create-form />
            </div>
        </div>
    </div>
</div>
<livewire:brand-create-table />
@endsection
