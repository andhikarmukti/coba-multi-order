@extends('layout.main')

@section('section')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-6 mx-auto">
            <div class="card shadow p-4">
                <h3 class="text-center mb-2">Tambah Produk Baru</h3>
                <livewire:product-create-form />
            </div>
        </div>
    </div>
    <livewire:product-create-table />
</div>


<script type="text/javascript">
    var capital_price = document.getElementById('capital_price');
    capital_price.addEventListener('keyup', function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        capital_price.value = formatRupiah(this.value);
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString()
            , split = number_string.split(',')
            , sisa = split[0].length % 3
            , capital_price = split[0].substr(0, sisa)
            , ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            capital_price += separator + ribuan.join('.');
        }

        capital_price = split[1] != undefined ? capital_price + ',' + split[1] : capital_price;
        return prefix == undefined ? capital_price : (capital_price ? '' + capital_price : '');
    }

</script>
<script type="text/javascript">
    var product_price = document.getElementById('product_price');
    product_price.addEventListener('keyup', function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        product_price.value = formatRupiah(this.value);
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString()
            , split = number_string.split(',')
            , sisa = split[0].length % 3
            , product_price = split[0].substr(0, sisa)
            , ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            product_price += separator + ribuan.join('.');
        }

        product_price = split[1] != undefined ? product_price + ',' + split[1] : product_price;
        return prefix == undefined ? product_price : (product_price ? '' + product_price : '');
    }

</script>

@endsection
