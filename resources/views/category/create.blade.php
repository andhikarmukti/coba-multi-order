@extends('layout.main')

@section('section')
    <livewire:category-create-form />
    <livewire:category-create-table />
@endsection
