<nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav m-auto">
                {{-- <a class="navbar-brand" href="#">Navbar</a>
                <li class="nav-item">
                    <a class="nav-link {{ $title == 'Home' ? 'active' : "" }}" aria-current="page" href="#">Home</a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link {{ $title == 'Penjualan' ? 'active' : "" }}" href="/penjualan">Penjualan</a>
                </li>
                @can('admin')
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Opsi
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/product">Tambah Product Baru</a></li>
                        <li><a class="dropdown-item" href="/brand">Tambah Brand Baru</a></li>
                        <li><a class="dropdown-item" href="/category">Tambah Category Baru</a></li>
                        <li><a class="dropdown-item" href="/variant">Tambah Variant Baru</a></li>
                        {{-- <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li> --}}
                    </ul>
                </li>
                @endcan
                {{-- <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>
