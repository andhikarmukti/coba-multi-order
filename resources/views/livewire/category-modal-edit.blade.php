<div>
    <form method="POST" wire:submit.prevent='update'>
        <h3 class="mb-4 text-center">Edit Category</h3>
        <div class="mb-3">
            <label for="category_name" class="form-label">Category name</label>
            <input type="text" class="form-control" id="category_name" name="category_name" wire:model='category_name'>
        </div>
        <div class="mb-3">
            <label class="form-label">Brand</label>
            <select wire:model="brand_id" class="form-select" name="brand_id">
                @foreach ($brands as $brand)
                    <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
