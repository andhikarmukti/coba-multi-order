<div>
    <div class="container">
        <div class="col-4 mx-auto">
            <div class="row">
                <div class="card shadow mt-5 p-4">
                    {{-- Alert --}}
                    @if (session()->has('store'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('store') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <form method="POST" wire:submit.prevent="store">
                        <div class="mb-3">
                            <label for="variant_name" class="form-label">Variant Name</label>
                            <input wire:model='variant_name' type="text" class="form-control" id="variant_name">
                        </div>
                        <div class="mb-3">
                            <label for="category" class="form-label">Category</label>
                            <select wire:model="category_id" class="form-select" id="category">
                                <option disabled>Open this select menu</option>
                                @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
