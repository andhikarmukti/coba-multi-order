<div>
    <div class="container">
        <div class="col-6 mx-auto">
            <div class="row">
                <div class="card shadow p-4 mt-5">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Variant Name</th>
                                <th scope="col">Category Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($variants as $variant)
                            <tr>
                                <th scope="row">{{ $variant->variant_name }}</th>
                                <td>{{ $variant->category_name }}</td>
                                <td>
                                    <button class="badge bg-warning border-0">edit</button>
                                    <button class="badge bg-danger border-0">delete</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
