<div>
    {{-- Alert --}}
    @if (session()->has('store'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('store') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <div class="card shadow p-4 mt-5 mb-5">
        <h3 class="text-center mb-3">Table Product</h3>
        <div class="col-4">
            <input wire:model="search" type="text" name="search" id="search" class="form-control mb-3"
                placeholder="search..">
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Nama Product</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Category</th>
                    <th scope="col">Variant</th>
                    <th scope="col" class="text-center">Harga Modal</th>
                    <th scope="col" class="text-center">Harga Jual</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                <tr>
                    <th scope="row">{{ $product->product_name }}</th>
                    <td>{{ $brand->find($product->brand_id)->brand_name }}</td>
                    <td>{{ $category->find($product->brand_id)->category_name }}</td>
                    <td>{{ $variant->find($product->brand_id)->variant_name }}</td>
                    <td class="text-center">Rp {{ number_format($product->capital_price,0,',','.') }}</td>
                    <td class="text-center">Rp {{ number_format($product->product_price,0,',','.') }}</td>
                    <td class="text-center">
                        <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                        <a href="#" class="badge bg-danger text-decoration-none">delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
