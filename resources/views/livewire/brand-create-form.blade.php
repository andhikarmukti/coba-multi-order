<div>
    <form method="POST" wire:submit.prevent="storeBrand">
        @csrf
        <h3>
            <div class="text-center mb-3">Tambah Brand Baru</div>
        </h3>
        <div class="mb-3">
            <label for="brand_name" class="form-label">Nama Brand</label>
            <input wire:model="brand_name" type="text" class="form-control @error('brand_name') is-invalid @enderror"
                id="brand_name" name="brand_name">
            @error('brand_name')
                <div class="invalid-feedback">
                    harus diisi, tidak boleh kosong!
                </div>
            @enderror
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
</div>
