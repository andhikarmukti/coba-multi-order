<div>
    <div class="container">
        <form wire:submit.prevent="storeOrder" method="POST">
            <div class="row">
                <div class="col-md-6">
                    <div class="card p-3 mb-5 mt-5 shadow">
                        <div class="row">
                            <div class="col-md">

                                {{-- Alert --}}
                                @if (session()->has('store'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {{ session('store') }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                @endif

                                <h3 class="text-center">Form Input Orderan</h3>
                                @csrf
                                <div class="row">
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label for="recipient" class="form-label">Penerima</label>
                                            <input wire:model='recipient' type="text"
                                                class="form-control @error('recipient') is-invalid @enderror"
                                                id="recipient" name="recipient">
                                            @error('recipient')
                                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                                    harus dipilih, tidak boleh kosong!
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label for="person_responsible" class="form-label ">Penanggung
                                                Jawab</label>
                                            <select wire:model='person_responsible'
                                                class="form-select @error('person_responsible') is-invalid @enderror"
                                                id="person_responsible" name="person_responsible">
                                                <option selected>--</option>
                                                @foreach ($staffs as $staff)
                                                    <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('person_responsible')
                                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                                    harus dipilih, tidak boleh kosong!
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label for="order_number" class="form-label">Nomor Order</label>
                                            <input wire:model='order_number' type="text"
                                                class="form-control @error('order_number') is-invalid @enderror"
                                                id="order_number" name="order_number">
                                            @error('order_number')
                                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                                    harus dipilih, tidak boleh kosong!
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label for="receipt_number" class="form-label">Nomor Resi</label>
                                            <input wire:model='receipt_number' type="text"
                                                class="form-control @error('receipt_number') is-invalid @enderror"
                                                id="receipt_number" name="receipt_number">
                                            @error('receipt_number')
                                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                                    harus dipilih, tidak boleh kosong!
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label for="quality_control" class="form-label">Quality Control</label>
                                            <select wire:model='quality_control'
                                                class="form-select @error('quality_control') is-invalid @enderror"
                                                id="quality_control" name="quality_control">
                                                <option selected>--</option>
                                                @foreach ($staffs as $staff)
                                                    <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('quality_control')
                                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                                    harus dipilih, tidak boleh kosong!
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div>
                                            <label for="packing" class="form-label">Packing</label>
                                            <select wire:model='packing'
                                                class="form-select @error('packing') is-invalid @enderror" id="packing"
                                                name="packing">
                                                <option selected>--</option>
                                                @foreach ($staffs as $staff)
                                                    <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('packing')
                                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                                    harus dipilih, tidak boleh kosong!
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-5 col-4 mx-auto text-center">
                                        <label for="status" class="form-label">Status</label>
                                        <select wire:model='status'
                                            class="form-select @error('status') is-invalid @enderror" id="status"
                                            name="status">
                                            <option selected>--</option>
                                            <option value="packing">Packing</option>
                                            <option value="selesai">Selesai</option>
                                        </select>
                                        @error('status')
                                            <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                                harus dipilih, tidak boleh kosong!
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card p-3 mb-5 mt-5 shadow">
                        <div class="col-md">
                            <h3 class="text-center">Item List</h3>
                            <div class="row">
                                <div class="col-9">
                                    <label for="product_id_0" class="form-label">Nama Barang</label>
                                    <select class="form-select @error('product_id_0') is-invalid @enderror"
                                        id="product_id_0" name="product_id_0">
                                        <option selected>Open this select menu</option>
                                        @foreach ($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('product_id_0')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            harus dipilih, tidak boleh kosong!
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-2">
                                    <label for="quantity_0" class="form-label">Quantity</label>
                                    <input type="number"
                                        class="form-control d-inline @error('quantity_0') is-invalid @enderror"
                                        id="quantity_0" name="quantity_0">
                                </div>
                            </div>
                            <div id="inputOrder"></div>
                            <div class="form-text mt-1"><button id="addMore" type="button" class="btn btn-link">Add
                                    more
                                    (+)</button></div>
                            <button type="submit" class="btn btn-primary mt-3">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
