<div>
    {{-- Alert --}}
    @if (session()->has('store'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('store') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <form wire:submit.prevent="storeProduct" method="POST">
        {{-- @csrf --}}
        <div class="mb-3">
            <label for="product_name" class="form-label">Nama Product</label>
            <input wire:model="product_name" type="text" class="form-control @error('product_name') is-invalid @enderror"
                id="product_name" name="product_name" value="{{ old('product_name') }}">
            @error('product_name')
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="brand_id" class="form-label">Brand</label>
            <select wire:model="brandId" class="form-select @error('brandId') is-invalid @enderror" id="brand_id"
                name="brand_id">
                <option selected>--</option>
                @foreach ($brands as $brand)
                    <option value="{{ $brand->id }}" @if (old('brandId')) selected @endif>{{ $brand->brand_name }}</option>
                @endforeach
            </select>
            @error('brandId')
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                    harus dipilih, tidak boleh kosong!
                </div>
            @enderror
        </div>
        @if ($categoryForm != null)
            <div class="mb-3">
                <label for="category_id" class="form-label">Category</label>
                <select wire:model="categoryId" class="form-select @error('category_id') is-invalid @enderror"
                    id="category_id" name="category_id">
                    <option selected>--</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                        harus dipilih, tidak boleh kosong!
                    </div>
                @enderror
            </div>
        @endif
        @if ($variantForm != null)
            <div class="mb-3">
                <label for="variant_id" class="form-label">Variant</label>
                <select wire:model="variant_id" class="form-select @error('variant_id') is-invalid @enderror"
                    id="variant_id" name="variant_id">
                    <option selected disabled>--</option>
                    @foreach ($variants as $variant)
                        <option value="{{ $variant->id }}">{{ $variant->variant_name }}</option>
                    @endforeach
                </select>
                @error('variant_id')
                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                        harus dipilih, tidak boleh kosong!
                    </div>
                @enderror
            </div>
        @endif
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="capital_price" class="form-label">Harga Modal</label>
                    <input wire:model="capital_price" type="text"
                        class="form-control @error('capital_price') is-invalid @enderror" id="capital_price"
                        name="capital_price">
                    @error('capital_price')
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            harus dipilih, tidak boleh kosong!
                        </div>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="product_price" class="form-label">Harga Jual</label>
                    <input wire:model="product_price" type="text"
                        class="form-control @error('product_price') is-invalid @enderror" id="product_price"
                        name="product_price">
                    @error('product_price')
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            harus dipilih, tidak boleh kosong!
                        </div>
                    @enderror
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
