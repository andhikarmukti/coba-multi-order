<div>
    <div class="container mb-5">
        <div class="col">
            <div class="card p-3 shadow mt-5">
                <h3 class="text-center">Tabel Orderan</h3>
                <table class="table table-hover mt-3">
                    <thead>
                        <tr>
                            <th scope="col">Penerima</th>
                            <th scope="col">Nomor Order</th>
                            <th scope="col">Nomor Resi</th>
                            <th scope="col">Status</th>
                            <th scope="col">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->recipient }}</td>
                                <td>{{ $item->order_number }}</td>
                                <td>{{ $item->receipt_number }}</td>
                                <td>{{ $item->status }}</td>
                                <td>
                                    <button type="button" class="badge bg-primary border-0 button_detail"
                                        data-bs-toggle="modal" data-bs-target="#exampleModal"
                                        value="{{ $item->order_number }}">detail</button>
                                    <button type="button" class="badge bg-warning border-0 button_detail"
                                        wire:click='selectedItem({{ $item->order_number }})'>bukti
                                        foto</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
