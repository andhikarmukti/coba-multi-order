<div>
    <div class="container">
        <div class="col-4 mx-auto">
            <div class="row">
                <div class="card shadow mt-5 p-4">
                    {{-- Alert --}}
                    @if (session()->has('store'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('store') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif

                    <form method="POST" wire:submit.prevent='store'>
                        <h3 class="mb-4 text-center">Tambah Category</h3>
                        <div class="mb-3">
                            <label for="category_name" class="form-label">Category name</label>
                            <input type="text" class="form-control" id="category_name" name="category_name"
                                wire:model='category_name'>
                        </div>
                        <div class="mb-3">
                            <label for="brand_id" class="form-label">Brand</label>
                            <select wire:model='brand_id' class="form-select" aria-label="Default select example">
                                <option selected>--</option>
                                @foreach ($brands as $brand)
                                <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
