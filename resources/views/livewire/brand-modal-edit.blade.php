<div>
    <form method="POST" onkeypress="return event.keyCode != 13">
        @csrf
        <h3>
            <div class="text-center mb-3">Edit Brand</div>
        </h3>
        <div class="mb-3">
            <label for="brand_name" class="form-label">Nama Brand</label>
            <input wire:model="brand_name" type="text" class="form-control @error('brand_name') is-invalid @enderror"
                id="brand_name" name="brand_name">
            @error('brand_name')
                <div class="invalid-feedback">
                    harus dipilih, tidak boleh kosong!
                </div>
            @enderror
        </div>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal" wire:click="update">Save
            Changes</button>
    </form>

    <script>
        window.addEventListener('openModalDelete', event => {
            $('#deleteModal').modal('show');
        })

        window.addEventListener('openModalEdit', event => {
            $('#editModal').modal('show');
        })

        window.addEventListener('closeModalEdit', event => {
            $('#editModal').modal('hide');
        })
    </script>
</div>
