<div>
    <div class="container">
        <div class="col-8 mx-auto">
            <div class=" row">
                <div class="card shadow p-4 mt-5">
                    <table class="table table-hover">
                        {{-- Alert --}}
                        @if (session()->has('category_edited'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            {{ session('category_edited') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        @if (session()->has('deleted_item'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('deleted_item') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        <thead>
                            <tr>
                                <th scope="col">Category Name</th>
                                <th scope="col">Brand</th>
                                <th scope="col" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                            <tr>
                                <th scope="row">{{ $category->category_name }}</th>
                                <td>{{ $category->brand_name }}</td>
                                <td class="text-center">
                                    <button wire:click="modal({{ $category->id }}, {{ $category->brand_id }}, 'edit')"
                                        class="badge bg-warning border-0">edit</button>
                                    <button wire:click="modal({{ $category->id }}, {{ $category->brand_id }},'delete')"
                                        class="badge bg-danger border-0">delete</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Edit --}}
    <div class="modal fade" tabindex="-1" id="editModalCategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <livewire:category-modal-edit />
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Delete-->
    <div class="modal fade" id="deleteModalCategory" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Are you sure?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button wire:click="delete" type="button" class="btn btn-danger"
                        data-bs-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        window.addEventListener('openModalEdit', event => {
            $('#editModalCategory').modal('show');
        })
        window.addEventListener('closeModalEdit', event => {
            $('#editModalCategory').modal('hide');
        })

        window.addEventListener('openModalDelete', event => {
            $('#deleteModalCategory').modal('show');
        })
    </script>
</div>
