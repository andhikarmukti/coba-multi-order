@extends('layout.main')

@section('section')
<div class="container">
    @can('admin')
    <form action="/penjualan" method="POST">
        <div class="row">
            <div class="col-md-6">
                <div class="card p-3 mb-5 mt-5 shadow">
                    <div class="row">
                        <div class="col-md">

                            {{-- Alert --}}
                            @if (session()->has('store'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('store') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                            @endif

                            <h3 class="text-center mb-5">Form Input Orderan</h3>
                            @csrf
                            <div class="row mb-2">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="recipient" class="form-label">Penerima</label>
                                        <input type="text" class="form-control @error('recipient') is-invalid @enderror"
                                            id="recipient" name="recipient">
                                        @error('recipient')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            harus dipilih, tidak boleh kosong!
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="person_responsible" class="form-label ">Penanggung Jawab</label>
                                        <select class="form-select @error('person_responsible') is-invalid @enderror"
                                            id="person_responsible" name="person_responsible">
                                            <option selected disabled>--</option>
                                            @foreach ($staffs as $staff)
                                            <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('person_responsible')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            harus dipilih, tidak boleh kosong!
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="order_number" class="form-label">Nomor Order</label>
                                        <input type="text"
                                            class="form-control @error('order_number') is-invalid @enderror"
                                            id="order_number" name="order_number">
                                        @error('order_number')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            harus dipilih, tidak boleh kosong!
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="receipt_number" class="form-label">Nomor Resi</label>
                                        <input type="text"
                                            class="form-control @error('receipt_number') is-invalid @enderror"
                                            id="receipt_number" name="receipt_number">
                                        @error('receipt_number')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            harus dipilih, tidak boleh kosong!
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label for="quality_control" class="form-label">Quality Control</label>
                                        <select class="form-select @error('quality_control') is-invalid @enderror"
                                            id="quality_control" name="quality_control">
                                            <option selected disabled>--</option>
                                            @foreach ($staffs as $staff)
                                            <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('quality_control')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            harus dipilih, tidak boleh kosong!
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div>
                                        <label for="packing" class="form-label">Packing</label>
                                        <select class="form-select @error('packing') is-invalid @enderror" id="packing"
                                            name="packing">
                                            <option selected disabled>--</option>
                                            @foreach ($staffs as $staff)
                                            <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('packing')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            harus dipilih, tidak boleh kosong!
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="mb-5 col-6 mx-auto">
                                    <label for="courier_id" class="form-label">Kurir</label>
                                    <select class="form-select @error('courier_id') is-invalid @enderror"
                                        id="courier_id" name="courier_id">
                                        <option selected disabled>--</option>
                                        @foreach ($couriers as $courier)
                                        <option value="{{ $courier->id }}">{{ $courier->courier_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('status')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        harus dipilih, tidak boleh kosong!
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-5 col-6 mx-auto">
                                    <label for="status" class="form-label">Status</label>
                                    <select class="form-select @error('status') is-invalid @enderror" id="status"
                                        name="status">
                                        <option selected disabled>--</option>
                                        <option value="packing">Packing</option>
                                        <option value="selesai">Selesai</option>
                                    </select>
                                    @error('status')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        harus dipilih, tidak boleh kosong!
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="card p-3 mb-5 mt-5 shadow">
                    <div class="col-md">
                        <h3 class="text-center mb-5">Item List</h3>
                        <div class="row">
                            <div class="col-9">
                                <label for="product_id_0" class="form-label">Nama Barang</label>
                                <select class="form-select @error('product_id_0') is-invalid @enderror"
                                    id="product_id_0" name="product_id_0">
                                    <option selected disabled>Open this select menu</option>
                                    @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                                    @endforeach
                                </select>
                                @error('product_id_0')
                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                    harus dipilih, tidak boleh kosong!
                                </div>
                                @enderror
                            </div>
                            <div class="col-2">
                                <label for="quantity_0" class="form-label">Quantity</label>
                                <input type="number"
                                    class="form-control d-inline @error('quantity_0') is-invalid @enderror"
                                    id="quantity_0" name="quantity_0">
                            </div>
                        </div>
                        <div id="inputOrder"></div>
                        <div class="form-text mt-1"><button id="addMore" type="button" class="btn btn-link">Add more
                                (+)</button></div>
                        <button type="submit" class="btn btn-primary mt-3">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endcan
</div>
</div>
<div class="container mb-5">
    <div class="col">
        <div class="card p-3 shadow mt-5">
            <h3 class="text-center mb-5">Tabel Orderan</h3>
            @if (session()->has('update_photo_proof'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('update_photo_proof') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <table class="table table-hover mt-3">
                <thead>
                    <tr>
                        <th scope="col">Penerima</th>
                        <th scope="col">Nomor Order</th>
                        <th scope="col">Nomor Resi</th>
                        <th scope="col">Status</th>
                        <th scope="col" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->recipient }}</td>
                        <td>{{ $item->order_number }}</td>
                        <td>{{ $item->receipt_number }}</td>
                        <td>{{ $item->status }}</td>
                        <td class="text-center">
                            <button type="button" class="badge bg-primary border-0 button_detail" data-bs-toggle="modal"
                                data-bs-target="#exampleModal" value="{{ $item->order_number }}">detail</button>
                            <button type="button" class="badge bg-warning border-0 button_detail photo_proof"
                                data-bs-toggle="modal" data-bs-target="#buktiFoto"
                                value="{{ $item->order_number }}">bukti
                                foto</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

{{-- Modal Detail --}}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Order</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body detail">
                <h5>Data Penerima</h5>
                <ul>
                    <li>Nama : Haura Razqya</li>
                    <li>Nomor Order : Haura Razqya</li>
                    <li>Nomor Resi : Haura Razqya</li>
                </ul>
                <hr>
                <h5>Detail Proses Order</h5>
                <ul>
                    <li>Penanggung Jawab : Haura Razqya</li>
                    <li>Quality Control : Haura Razqya</li>
                    <li>Packing : Haura Razqya</li>
                </ul>
                <hr>
                <h5>Detail Barang</h5>
                <ul class="detail_barang">
                    <li>Nama Barang : Barang 1 | Qty : 4</li>
                    <li>Nama Barang : Barang 2 | Qty : 2</li>
                    <li>Nama Barang : Barang 3 | Qty : 1</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{-- Modal Bukti Foto --}}
<div class="modal fade" id="buktiFoto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Upload Bukti Foto</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/penjualan/update" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" value="" name="order_number" id="modal_order_id">
                    <input type="text" class="form-control @error('photo_proof')
                        is-invalid
                    @enderror" placeholder="insert link.." name="photo_proof">
                    @error('photo_proof')
                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                        tidak boleh kosong!
                    </div>
                    @enderror
                    <a href="" class="cek-foto badge bg-success text-decoration-none submitted mt-3" target="_blank">Cek
                        bukti foto</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Script add more input barang --}}
<script>
    let i = 0;
        $('#addMore').click(function() {
            i++;
            let inputBaru = `<div class="addOrder row">
                                <div class="col-9">
                                    <label for="product_id_` + i + `" class="form-label mt-3">Nama Barang</label>
                                    <select class="form-select" id="product_id_` + i + `" name="product_id_` + i + `">
                                        <option selected disabled>Open this select menu</option>
                                        @foreach ($products as $product)
                                            <option value="` + " {{ $product->id }}" + `">` + "{{ $product->product_name }}" + `</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-2">
                                    <label for="quantity_` + i + `" class="form-label mt-3">Quantity</label>
                                    <input type="number" class="form-control" id="quantity_` + i +
                `" name="quantity_` + i + `">
                                </div>
                                <div class="col-1 mt-5">
                                    <button type="button" class="btn border-0 close"><i class="bi bi-x-circle text-danger fs-5"></i></button>
                                </div>
                            </div>`
            $(inputBaru).insertBefore('#inputOrder');
        });

        $('body').click(function() {
            $('.close').click(function() {
                $(this).parents('.addOrder').remove();
            });
        });
</script>

{{-- Script get data modal --}}
<script>
    $('.button_detail').click(function() {
            const order_number = $(this).val();
            $.ajax({
                type: 'GET',
                url: "/penjualan?order_number=" + order_number,
                dataType: 'JSON',
                success: function(result) {
                    $('.modal-body detail').html(
                        `
                        <div class="modal-body">
                            <h5>Data Penerima</h5>
                            <ul>
                                <li>Nama : <p style="font-weight: bold">` + result['orders'][0]['recipient'] + `</p></li>
                                <li>Nomor Order : <p style="font-weight: bold">` + result['orders'][0][
                            'order_number'
                        ] + `</p></li>
                                <li>Nomor Resi : <p style="font-weight: bold">` + result['orders'][0][
                            'receipt_number'
                        ] + `</p></li>
                            </ul>
                            <hr>
                            <h5>Detail Proses Order</h5>
                            <ul>
                                <li>Penanggung Jawab : <p style="font-weight: bold" class="d-inline">` + result[
                            'person_responsible']['name'] + `</p></li>
                                <li>Quality Control : <p style="font-weight: bold" class="d-inline">` + result[
                            'quality_control']['name'] + `</p></li>
                                <li>Packing : <p style="font-weight: bold" class="d-inline">` + result['packing'][
                            'name'
                        ] + `</p></li>
                            </ul>
                            <hr>
                            <h5>Detail Barang</h5>
                            <ol class="detail_barang"></ol>
                        </div>
                    `
                    )
                    $('.detail_barang').empty();
                    result['orders'].forEach(element => {
                        $('.detail_barang').append(`
                        <li>` + element['product_name'] + ` | Qty : ` + element['quantity'] + `</li>
                    `);
                    });
                }
            });
        });
</script>

<script>
    $('.photo_proof').click(function() {
            const order_number = $(this).val();
            $.ajax({
                type: 'GET',
                url: "/penjualan/update?order_number=" + order_number,
                dataType: 'JSON',
                success: function(result) {
                    $('#modal_order_id').val(result['order_number']);
                    $('.cek-foto').attr('href', result['photo_proof']);
                    if (result['photo_proof'] == null) {
                        $('.submitted').prop('hidden', !this.checked);
                    } else {
                        $('.submitted').removeAttr('hidden');
                    }
                }
            })
        })
</script>
@endsection
