@extends('layout.main')

@section('section')
    <livewire:variant-create-form />
    <livewire:variant-create-table />
@endsection
