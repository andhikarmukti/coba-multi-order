<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\VariantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/penjualan');
});
require __DIR__ . '/auth.php';

// Penjualan
Route::get('/penjualan', [OrderController::class, 'create'])->middleware('auth');
Route::post('/penjualan', [OrderController::class, 'store'])->middleware('auth');
Route::post('/penjualan/update', [OrderController::class, 'update'])->middleware('auth');
Route::get('/penjualan/update', [OrderController::class, 'jqueryGetIdOrder'])->middleware('auth'); // ajax pengambilan id untuk bukti foto

Route::group(['middleware' => 'admin'], function () {
    // Product
    Route::get('/product', [ProductController::class, 'create'])->middleware('auth');
    // Route::post('/product', [ProductController::class, 'store']);

    // Brand
    Route::get('/brand', [BrandController::class, 'create'])->middleware('auth');
    // Route::post('/brand', [BrandController::class, 'store'])->middleware('auth');

    // Category
    Route::get('/category', [CategoryController::class, 'create'])->middleware('auth');

    // Variant
    Route::get('/variant', [VariantController::class, 'create'])->middleware('auth');
});
