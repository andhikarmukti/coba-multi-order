<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'product_name' => 'Product A1',
            'brand_id' => 1,
            'category_id' => 1,
            'variant_id' => 1,
            'capital_price' => 500,
            'product_price' => 1000
        ]);
        Product::create([
            'product_name' => 'Product B1',
            'brand_id' => 1,
            'category_id' => 1,
            'variant_id' => 2,
            'capital_price' => 500,
            'product_price' => 2000
        ]);
        Product::create([
            'product_name' => 'Product C1',
            'brand_id' => 1,
            'category_id' => 2,
            'variant_id' => 1,
            'capital_price' => 500,
            'product_price' => 3000
        ]);
        Product::create([
            'product_name' => 'Product D1',
            'brand_id' => 1,
            'category_id' => 2,
            'variant_id' => 2,
            'capital_price' => 500,
            'product_price' => 4000
        ]);
        Product::create([
            'product_name' => 'Product A2',
            'brand_id' => 2,
            'category_id' => 3,
            'variant_id' => 3,
            'capital_price' => 500,
            'product_price' => 5000
        ]);
        Product::create([
            'product_name' => 'Product B2',
            'brand_id' => 2,
            'category_id' => 3,
            'variant_id' => 4,
            'capital_price' => 500,
            'product_price' => 6000
        ]);
        Product::create([
            'product_name' => 'Product C2',
            'brand_id' => 2,
            'category_id' => 4,
            'variant_id' => 5,
            'capital_price' => 500,
            'product_price' => 7000
        ]);
        Product::create([
            'product_name' => 'Product D2',
            'brand_id' => 2,
            'category_id' => 4,
            'variant_id' => 6,
            'capital_price' => 500,
            'product_price' => 8000
        ]);
    }
}
