<?php

namespace Database\Seeders;

use App\Models\Variant;
use Illuminate\Database\Seeder;

class VariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Variant::create([
            'category_id' => 1,
            'variant_name' => 'Variant A1'
        ]);
        Variant::create([
            'category_id' => 1,
            'variant_name' => 'Variant B1'
        ]);
        Variant::create([
            'category_id' => 2,
            'variant_name' => 'Variant A2'
        ]);
        Variant::create([
            'category_id' => 2,
            'variant_name' => 'Variant B2'
        ]);
        Variant::create([
            'category_id' => 3,
            'variant_name' => 'Variant A3'
        ]);
        Variant::create([
            'category_id' => 3,
            'variant_name' => 'Variant B3'
        ]);
        Variant::create([
            'category_id' => 4,
            'variant_name' => 'Variant A4'
        ]);
        Variant::create([
            'category_id' => 4,
            'variant_name' => 'Variant B4'
        ]);
    }
}
