<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'brand_id' => 1,
            'category_name' => 'Category A1'
        ]);
        Category::create([
            'brand_id' => 1,
            'category_name' => 'Category B1'
        ]);
        Category::create([
            'brand_id' => 2,
            'category_name' => 'Category A2'
        ]);
        Category::create([
            'brand_id' => 2,
            'category_name' => 'Category B2'
        ]);
    }
}
