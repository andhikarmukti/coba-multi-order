<?php

namespace Database\Seeders;

use App\Models\Staff;
use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Staff::create([
            'name' => 'Dika',
            'status' => 'Staff'
        ]);
        Staff::create([
            'name' => 'Udin',
            'status' => 'Magang'
        ]);
        Staff::create([
            'name' => 'Mamat',
            'status' => 'Magang'
        ]);
    }
}
